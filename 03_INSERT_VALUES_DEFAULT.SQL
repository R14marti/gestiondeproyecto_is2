--INSERT DE PERMISOS PREDEFINIDOS
INSERT INTO PERMISO (ID_PERMISO,
                     DESCRIPCION_PERMISO,
                     EDICION,
                     VISUALIZACION,
                     BORRADO)
VALUES (1,'FULL','SI','SI','SI');

INSERT INTO PERMISO (ID_PERMISO,
                     DESCRIPCION_PERMISO,
                     EDICION,
                     VISUALIZACION,
                     BORRADO)
VALUES (2,'SOLO EDICIÓN','SI','SI','NO');

INSERT INTO PERMISO (ID_PERMISO,
                     DESCRIPCION_PERMISO,
                     EDICION,
                     VISUALIZACION,
                     BORRADO)
VALUES (3,'SOLO VISUALIZACIÓN','NO','SI','NO');

--INSERT DE ESTADOS KANBAN
INSERT INTO ESTADO_KANBAN (ID_ESTADO_KANBAN, DESCRIPCION)
VALUES (1,'POR HACER');

INSERT INTO ESTADO_KANBAN (ID_ESTADO_KANBAN, DESCRIPCION)
VALUES (2,'HACIENDO');

INSERT INTO ESTADO_KANBAN (ID_ESTADO_KANBAN, DESCRIPCION)
VALUES (3,'TERMINADA');

--INSERT DE ESTADOS DE PROYECTOS
INSERT INTO ESTADO_PROYECTO (ID_ESTADO, DESCRIPCION)
VALUES ('PEN', 'PENDIENTE');
INSERT INTO ESTADO_PROYECTO (ID_ESTADO, DESCRIPCION)
VALUES ('TER','TERMINADO');
INSERT INTO ESTADO_PROYECTO (ID_ESTADO, DESCRIPCION)
VALUES ('CUR','EN CURSO');
INSERT INTO ESTADO_PROYECTO (ID_ESTADO, DESCRIPCION)
VALUES ('CAN','CANCELADO');

--INSERT DE PRIORIDADES
INSERT INTO prioridad (id,
                       descripcion,
                       meta)
VALUES (1,'Urgente','0.45');
INSERT INTO prioridad (id,
                       descripcion,
                       meta)
VALUES (2,'Crítico','1');
INSERT INTO prioridad (id,
                       descripcion,
                       meta)
VALUES (3,'Trivial','2');
INSERT INTO prioridad (id,
                       descripcion,
                       meta)
VALUES (4,'Menor','3');

commit;

