CREATE TABLE estado_kanban (
	id_estado_kanban     NUMBER(2) NOT NULL ,
	descripcion			 VARCHAR2(100) NOT NULL,
                CONSTRAINT ESTADO_KANBAN_PK PRIMARY KEY (id_estado_kanban)
);


CREATE TABLE estado_proyecto (
                id_estado VARCHAR2(3) NOT NULL,
                descripcion VARCHAR2(20) NOT NULL,
                CONSTRAINT ESTADO_PROYECTO_PK PRIMARY KEY (id_estado)
);


CREATE TABLE proyecto (
                id_proyecto VARCHAR2(10) NOT NULL,
                nombre VARCHAR2(100) NOT NULL,
                descripcion VARCHAR2(500) NOT NULL,
                fecha_inicio DATE NOT NULL,
                fecha_fin DATE,
                estado VARCHAR2(3)  DEFAULT 'PEN' NOT NULL,
                CONSTRAINT PROYECTO_PK PRIMARY KEY (id_proyecto)
);

ALTER TABLE proyecto
	add constraint R_ESTADO_PROYECTO FOREIGN KEY (estado)
	REFERENCES estado_proyecto (id_estado);

alter table proyecto
  add constraint CK_ESTADO_CHEK1
  check (estado IN ('PEN', 'CAN','TER','CUR'));
  
ALTER TABLE proyecto
ADD CONSTRAINTS CK_FECHA_FIN_INICIO_CHEK2
CHECK (fecha_fin>= fecha_inicio);


CREATE TABLE sprint_backlog (
                id_sprint NUMBER NOT NULL,
                nombre VARCHAR2(100) NOT NULL,
                fecha_inicio DATE NOT NULL,
                fecha_fin DATE,
                id_proyecto VARCHAR2(10) NOT NULL,
                CONSTRAINT SPRINT_BACKLOG_PK PRIMARY KEY (id_sprint)
);

ALTER TABLE sprint_backlog
ADD CONSTRAINTS CK_FECHA_FIN_INICIO_CHEK3
CHECK (fecha_fin>= fecha_inicio);

CREATE TABLE permiso (
                id_permiso NUMBER NOT NULL,
                descripcion_permiso VARCHAR2(100) NOT NULL,
                edicion CHAR(2) DEFAULT 'NO' NOT NULL CONSTRAINT CK_EDICION_CHECK2 CHECK (edicion IN ('SI', 'NO')),
                visualizacion CHAR(2) DEFAULT 'NO' NOT NULL CONSTRAINT CK_VISUALIZACION_CHECK3 CHECK (visualizacion IN ('SI', 'NO')),
                borrado CHAR(2) DEFAULT 'NO' NOT NULL CONSTRAINT CK_BORRADO_CHECK4 CHECK (borrado IN ('SI', 'NO')),
                CONSTRAINT PERMISO_PK PRIMARY KEY (id_permiso)
);

CREATE TABLE rol_sistema (
                id_rol NUMBER NOT NULL,
                descripcion VARCHAR2(100) NOT NULL,
				id_permiso number default 2,
                CONSTRAINT ROL_SISTEMA_PK PRIMARY KEY (id_rol)
);

ALTER TABLE rol_sistema ADD CONSTRAINT R_PERMISO_ROL FOREIGN KEY (id_permiso)
REFERENCES permiso (id_permiso);



CREATE TABLE usuario (
                nombre_usuario VARCHAR2(15) NOT NULL,
                nombre VARCHAR2(100) NOT NULL,
                apellido VARCHAR2(100) NOT NULL,
                contrasenha VARCHAR2(10) NOT NULL,
                correo VARCHAR2(30) NOT NULL,
				estado number(1) default 1 not null constraint CK_ESTADO_USER_CHEK5 CHECK (estado in (1,0)),
                CONSTRAINT USUARIO_PK PRIMARY KEY (nombre_usuario)
);

CREATE TABLE user_histories (
                id_us NUMBER NOT NULL,
				resumen varchar2(15) not null,
				prioridad varchar2(30) not null,
                descripcion VARCHAR2(100) NOT NULL,
				fecha_creacion date not null,
                estado_kanban NUMBER(2) NOT NULL,
                us_sprint NUMBER,
				id_proyecto varchar2(10) not null,
                CONSTRAINT USER_HISTORIES_PK PRIMARY KEY (id_us)
);

CREATE TABLE user_rol_us_histories (
                id NUMBER NOT NULL,
				id_rol NUMBER NOT NULL,
				nombre_usuario VARCHAR2(15) NOT NULL,
				id_us NUMBER NOT NULL,
				CONSTRAINT ID_USER_ROL_US_HISTORIES_PK PRIMARY KEY (id)
);
ALTER TABLE user_rol_us_histories ADD CONSTRAINT R_ID_ROL
FOREIGN KEY (id_rol)
REFERENCES rol_sistema (id_rol);

ALTER TABLE user_rol_us_histories ADD CONSTRAINT R_NOMBRE_USUARIO
FOREIGN KEY (nombre_usuario)
REFERENCES usuario (nombre_usuario);

ALTER TABLE user_rol_us_histories ADD CONSTRAINT R_USER_HISTORIE
FOREIGN KEY (id_us)
REFERENCES user_histories (id_us);

ALTER TABLE user_histories ADD CONSTRAINT R_ESTADO_KANBAN
FOREIGN KEY (estado_kanban)
REFERENCES estado_kanban (id_estado_kanban);


ALTER TABLE user_histories ADD CONSTRAINT R_SPRINT_USER_HISTORIES
FOREIGN KEY (us_sprint)
REFERENCES sprint_backlog (id_sprint);

ALTER TABLE user_histories ADD CONSTRAINT R_PROYECTO_USER_HISTORIES
FOREIGN KEY (id_proyecto)
REFERENCES proyecto (id_proyecto);


--Secuencia para la tabla USER_HISTORIES
CREATE SEQUENCE S_US MINVALUE 1 START WITH 1INCREMENT BY 1 NOCACHE;
--Secuencia para la tabla SPRINT_BACKLOG
CREATE SEQUENCE S_SPB MINVALUE 1 START WITH 1INCREMENT BY 1 NOCACHE;
--Secuencia para la tabla user_rol_us_histories
CREATE SEQUENCE S_URUS MINVALUE 1 START WITH 1INCREMENT BY 1 NOCACHE;

ALTER TABLE SPRINT_BACKLOG ADD ( FECHA_ESTIMADA_DE_FINALIZACION DATE NOT NULL);

ALTER TABLE user_histories MODIFY (prioridad number );

CREATE TABLE prioridad (
                id NUMBER NOT NULL,
				descripcion VARCHAR2 (15) NOT NULL,
				meta VARCHAR2(5) NOT NULL,
				CONSTRAINT id_prioridad_pk PRIMARY KEY (id)
);

ALTER TABLE user_histories ADD CONSTRAINT R_PRIORIDAD
FOREIGN KEY (prioridad)
REFERENCES prioridad (id);


