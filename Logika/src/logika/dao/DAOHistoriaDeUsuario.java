package logika.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import logika.model.HistoriaDeUsuario;



public class DAOHistoriaDeUsuario {
	public DAOHistoriaDeUsuario() {}
	public ArrayList<HistoriaDeUsuario> getHistoriaDeUsuario() throws SQLException {
		ArrayList<HistoriaDeUsuario> result = new ArrayList<HistoriaDeUsuario>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select uh.*, r.nombre_usuario from user_histories uh  left join user_rol_us_histories r on uh.id_us= r.id_us ");
		while(rs.next()) {
			HistoriaDeUsuario it = new HistoriaDeUsuario();
			it.setIdHistoria(rs.getInt("id_us"));
			it.setResumen(rs.getString("resumen"));
			it.setPrioridad(rs.getString("prioridad"));
			it.setDescripcion(rs.getString("descripcion"));
			it.setEstadoKamban(rs.getInt("estado_kanban"));
			it.setIdSprint(rs.getInt("us_sprint"));
			it.setFecha_creacion(rs.getDate("fecha_creacion"));
			it.setIdProyecto(rs.getString("id_proyecto"));
			it.setNombreUsuario(rs.getString("nombre_usuario"));
			
			result.add(it);
		}
		return result;
	}
	

	public ArrayList<String> getNombreSprint() throws SQLException {
		ArrayList<String> result = new ArrayList<String>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from sprint_backlog");
		while(rs.next()) {
			result.add(rs.getString("nombre"));
		}
		return result;
	} 
	
	public ArrayList<String> getNombreProyecto() throws SQLException {
		ArrayList<String> result = new ArrayList<String>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from proyecto");
		while(rs.next()) {
			result.add(rs.getString("nombre"));
		}
		return result;
	}
	
	public ArrayList<String> getResponsable() throws SQLException {
		ArrayList<String> result = new ArrayList<String>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from usuario");
		while(rs.next()) {
			result.add(rs.getString("nombre_usuario"));
		}
		return result;
	}
	
	public ArrayList<String> getRol() throws SQLException {
		ArrayList<String> result = new ArrayList<String>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from rol_sistema");
		while(rs.next()) {
		
			result.add(rs.getString("descripcion"));
		}
		
		return result;
	}
	

	public void guardar(HistoriaDeUsuario histories) throws SQLException {
		ConnectionOracle bd = new ConnectionOracle();
		ArrayList<HistoriaDeUsuario> history = new ArrayList<HistoriaDeUsuario>();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		boolean flag= false;
		history = getHistoriaDeUsuario();
		System.out.println("1");
		for (HistoriaDeUsuario p : history) {
			System.out.println("2");
			if (p.getIdHistoria() == histories.getIdHistoria()){
				System.out.println("3");
				flag= true;
			}
		}
		
		
        
		if(!flag) {
			System.out.println("histories.getResumen() " + histories.getResumen());
			System.out.println("histories.getPrioridad() " + histories.getPrioridad());
			System.out.println("histories.getDescripcion() " + histories.getDescripcion());
			System.out.println("histories.getEstadoKamban() " + histories.getEstadoKamban());
			System.out.println("histories.getIdSprint() " + histories.getIdSprint());
			System.out.println("histories.getIdProyecto() " + histories.getIdProyecto());
			st.executeUpdate("insert into user_histories (id_us, resumen, prioridad, descripcion,estado_kanban,  us_sprint, fecha_creacion, id_proyecto)  values (S_US.NEXTVAL,'"+histories.getResumen()+"', '"+histories.getPrioridad()+"','"+histories.getDescripcion()+"', '"+histories.getEstadoKamban()+"', (select id_sprint from sprint_backlog where nombre= '"+histories.getIdSprint()+"'), SYSDATE, (select id_proyecto from proyecto where nombre='"+histories.getIdProyecto()+"'))");
			st.executeUpdate("insert into user_rol_us_histories (id, id_rol, nombre_usuario, id_us) values (S_URUS.NEXTVAL, (select id_rol from rol_sistema where descripcion = '"+histories.getRol()+"'), '"+histories.getNombreUsuario()+"',   S_US.CURRVAL)");
			}else{
			/*System.out.println("proyects.getNombre(): "+proyects.getNombre()+"- proyects.getDescripcion(): "+proyects.getDescripcion()+"- fInicio: "+
		fInicio+" - fFin: "+fFin+" - proyects.getEstado(): "+proyects.getEstado()+" - proyects.getIdProyecto(): "+proyects.getIdProyecto());*/
			/*st.executeUpdate("update proyecto set nombre='"+ proyects.getNombre()+"', descripcion='"+proyects.getDescripcion()+"', "
			+ "fecha_inicio=to_date('"+fInicio+"','DD/MM/YYYY'), fecha_fin= to_date('"+fFin+"','DD/MM/YYYY'), estado='"+ proyects.getEstado()+"' where id_Proyecto='"+proyects.getIdProyecto()+"'");*/
		}
		conn.commit();
		conn.close();
	}

}
