package logika.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import logika.model.Permiso;
import logika.model.Rol;
//comentario
public class DAORol {
	
	public DAORol() {}

	public ArrayList<Rol> getRoles() throws SQLException {
		ArrayList<Rol> result = new ArrayList<Rol>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select r.*, p.descripcion_permiso from rol_sistema r join permiso p on r.id_permiso=p.id_permiso");
		while(rs.next()) {
			Rol it = new Rol();
			it.setCodigo(rs.getInt("id_rol"));
			it.setDescripcion(rs.getString("descripcion"));
			it.setPermiso(new Permiso());
			it.getPermiso().setId(rs.getLong("id_permiso"));
			it.getPermiso().setDescripcion(rs.getString("descripcion_permiso"));
			result.add(it);
		}
		return result;
	}
	
	public ArrayList<Permiso> getPermisos() throws SQLException {
		ArrayList<Permiso> result = new ArrayList<Permiso>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from permiso");
		while(rs.next()) {
			Permiso it = new Permiso();
			it.setId(rs.getInt("id_permiso"));
			it.setDescripcion(rs.getString("descripcion_permiso"));
			result.add(it);
		}
		return result;
	}

	public void guardar(Rol rol) throws SQLException {
		ConnectionOracle bd = new ConnectionOracle();
		ArrayList<Rol> roles = new ArrayList<Rol>();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = null;		
		boolean flag= false;
		int id=0;
		
		roles = getRoles();
		for (Rol u : roles) {
			if (rol.getDescripcion().equals(u.getDescripcion())){
				flag= true;
			}
		}
		if(!flag) {
			rs = st.executeQuery("select nvl(max(id_rol),0)+1 from rol_sistema");
			if (rs.next()) {
				id = rs.getInt(1);
			}
			st.executeUpdate("insert into rol_sistema (id_rol, descripcion, id_permiso) values ("+id+", '"+rol.getDescripcion()+"', "+rol.getPermiso().getId()+")");			
		}else{
			System.out.println("update rol_sistema set descripcion='"+rol.getDescripcion()+"', id_permiso="+rol.getPermiso().getId()+" where id_rol="+rol.getCodigo());
			st.executeUpdate("update rol_sistema set descripcion='"+rol.getDescripcion()+"', id_permiso="+rol.getPermiso().getId()+" where id_rol="+rol.getCodigo());
		}
		conn.commit();
		conn.close();
	}
}
