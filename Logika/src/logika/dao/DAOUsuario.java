package logika.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import logika.model.Permiso;
import logika.model.Usuario;

public class DAOUsuario {
	
	public DAOUsuario() {}

	public ArrayList<Usuario> getUsuarios() throws SQLException {
		ArrayList<Usuario> result = new ArrayList<Usuario>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from usuario");
		while(rs.next()) {
			Usuario it = new Usuario();
			it.setNickName(rs.getString("nombre_usuario"));
			it.setNombre(rs.getString("nombre"));
			it.setApellido(rs.getString("apellido"));
			it.setContrasenha(rs.getString("contrasenha"));
			it.setEmail(rs.getString("correo"));
			it.setEstado(rs.getBoolean("estado"));
			result.add(it);
		}
		return result;
	}

	public void guardar(Usuario user) throws SQLException {
		ConnectionOracle bd = new ConnectionOracle();
		ArrayList<Usuario> users = new ArrayList<Usuario>();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		boolean flag= false;
		
		users = getUsuarios();
		for (Usuario u : users) {
			if (user.getNickName().equals(u.getNickName())){
				flag= true;
			}
		}
		
		if(!flag) {
			st.executeUpdate("insert into usuario (nombre_usuario, nombre, apellido, contrasenha, correo, estado) values (LOWER('"+user.getNickName()+"'),'"
			+user.getNombre()+"','"+user.getApellido()+"','"+user.getContrasenha()+"', '"+user.getEmail()+"', "+(user.isEstado()?1:0)+")");			
		}else{
			st.executeUpdate("update usuario set nombre='"+user.getNombre()+"', apellido='"+user.getApellido()
			+"', contrasenha='"+user.getContrasenha()+"', correo='"+user.getEmail()+"', estado="+(user.isEstado()?1:0)
			+" where nombre_usuario='"+user.getNickName()+"'");
		}
		conn.commit();
		conn.close();
	}
	
	public Permiso getPermisoUsuario(String nick) throws SQLException {
		Permiso result = new Permiso();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from (select r.id_permiso, p.descripcion_permiso "
		+ "from rol_sistema r join permiso p on r.id_permiso=p.id_permiso " 
		+ "where exists(select 'x' from user_rol_us_histories urh " 
		+ "where urh.id_rol=r.id_rol and urh.nombre_usuario='"+nick+"') order by 1 desc) where rownum<2");
		if(rs.next()) {
			result.setId(rs.getLong("id_permiso"));
			result.setDescripcion(rs.getString("descripcion_permiso"));
		}else{
			result.setId(3);
			result.setDescripcion("SOLO VISUALIZACIÓN");
		}
		return result;
	}
}
