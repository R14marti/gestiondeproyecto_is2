package logika.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import logika.model.Proyecto;
import logika.model.Sprint;


public class DAOSprint {
	
	public DAOSprint() {}

	public ArrayList<Sprint> getSprints() throws SQLException {
		ArrayList<Sprint> result = new ArrayList<Sprint>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select sb.*, p.descripcion as proyecto from sprint_backlog sb "
		+ "join proyecto p on sb.id_proyecto=p.id_proyecto");
		while(rs.next()) {
			Sprint it = new Sprint();
			it.setIdSprint(rs.getLong("id_sprint"));
			it.setNombre(rs.getString("nombre"));
			it.setFechaInicio(rs.getDate("fecha_inicio"));
			it.setFechaFin(rs.getDate("fecha_fin"));
			it.setFechaFinEstimada(rs.getDate("fecha_estimada_de_finalizacion"));
			it.setIdProyecto(rs.getString("id_proyecto"));
			it.setDescProyecto(rs.getString("proyecto"));
			result.add(it);
		}
		return result;
	}
	
	public java.sql.Date convertir(java.util.Date fechaUtilDate){
	    return new java.sql.Date(fechaUtilDate.getTime());
	}

	public void guardar(Sprint pSprint) throws SQLException {
		ConnectionOracle bd = new ConnectionOracle();
		ArrayList<Sprint> listSprint = new ArrayList<Sprint>();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		boolean flag= false;
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		java.sql.Date fechaconvert = convertir(pSprint.getFechaInicio());
		java.sql.Date fechaconvertfin = convertir(pSprint.getFechaFin());
		java.sql.Date fechaconvertfinEst = convertir(pSprint.getFechaFinEstimada());
        String fInicio = formatter.format(fechaconvert);
        String fFin = formatter.format(fechaconvertfin);
        String fFinEst = formatter.format(fechaconvertfinEst);
		
		listSprint = getSprints();
		for (Sprint p : listSprint) {
			if (p.getIdSprint() == (pSprint.getIdSprint())){
				flag= true;
			}
		}

		if(!flag) {
			System.out.println("insert");
			st.executeUpdate("insert into sprint_backlog (id_sprint, nombre, fecha_inicio, fecha_fin, id_proyecto, fecha_estimada_de_finalizacion) values (S_SPB.nextval,'"
		+pSprint.getNombre()+"', to_date('"+fInicio+"','DD/MM/YYYY'), to_date('"+fFin+"','DD/MM/YYYY'), '"+pSprint.getDescProyecto().split("-")[0]+"', to_date('"+fFinEst+"','DD/MM/YYYY'))");
		}else{
			System.out.println("insert");
			st.executeUpdate("update sprint_backlog set nombre='"+ pSprint.getNombre()+"', fecha_inicio=to_date('"+fInicio+"','DD/MM/YYYY'), "
			+ "fecha_fin= to_date('"+fFin+"','DD/MM/YYYY'), id_proyecto='"+pSprint.getDescProyecto().split("-")[0]+"', fecha_estimada_de_finalizacion = to_date('"+fFinEst+"','DD/MM/YYYY') where id_sprint="+pSprint.getIdSprint());
		}
		conn.commit();
		conn.close();
	}
	
	public ArrayList<String> completeProyecto(String param) throws SQLException {
		ArrayList<String> result = new ArrayList<String>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from proyecto where estado in ('PEN') and descripcion like '%"+param+"%'");
		while(rs.next()) {						
			result.add(rs.getString("id_proyecto")+"-"+rs.getString("descripcion"));
		}
		return result;
	}
}
