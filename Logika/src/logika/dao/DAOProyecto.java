package logika.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import logika.model.Proyecto;


public class DAOProyecto {
	public DAOProyecto() {}

	public ArrayList<Proyecto> getProyecto() throws SQLException {
		ArrayList<Proyecto> result = new ArrayList<Proyecto>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from proyecto");
		while(rs.next()) {
			Proyecto it = new Proyecto();
			it.setIdProyecto(rs.getString("id_proyecto"));
			it.setNombre(rs.getString("nombre"));
			it.setDescripcion(rs.getString("descripcion"));
			it.setFechaInicio(rs.getDate("fecha_inicio"));
			it.setFechaFin(rs.getDate("fecha_fin"));
			it.setEstado(rs.getString("estado"));
			result.add(it);
		}
		return result;
	}
	
	public java.sql.Date convertir(java.util.Date fechaUtilDate){
	    return new java.sql.Date(fechaUtilDate.getTime());
	}

	public void guardar(Proyecto proyects) throws SQLException {
		ConnectionOracle bd = new ConnectionOracle();
		ArrayList<Proyecto> proyect = new ArrayList<Proyecto>();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		boolean flag= false;
		proyect = getProyecto();
		System.out.println("2");
		for (Proyecto p : proyect) {
			if (p.getIdProyecto().equals(proyects.getIdProyecto())){
				flag= true;
			}
		}
		
		
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		java.sql.Date fechaconvert =  convertir(proyects.getFechaInicio());
		java.sql.Date fechaconvertfin =  convertir(proyects.getFechaFin());
        String fInicio = formatter.format(fechaconvert);
        String fFin = formatter.format(fechaconvertfin);
        System.out.println("3");
        
		if(!flag) {
			System.out.println("insert into proyecto (id_proyecto, nombre, descripcion, fecha_inicio, fecha_fin, estado) values ('"+proyects.getIdProyecto()+"','"+ 
					proyects.getNombre()+"','"+proyects.getDescripcion()+"', to_date('"+fInicio+"','DD/MM/YYYY'), to_date('"+fFin+"','DD/MM/YYYY')" + 
					",(select id_estado from estado_proyecto where descripcion='"+proyects.getEstado()+"'));");
			st.executeUpdate("insert into proyecto (id_proyecto, nombre, descripcion, fecha_inicio, fecha_fin, estado) values ('"+proyects.getIdProyecto()+"','"+ 
					proyects.getNombre()+"','"+proyects.getDescripcion()+"', to_date('"+fInicio+"','DD/MM/YYYY'), to_date('"+fFin+"','DD/MM/YYYY')" + 
					",(select id_estado from estado_proyecto where descripcion='"+proyects.getEstado()+"'))");		
		}else{
			System.out.println("update proyecto set nombre='"+ proyects.getNombre()+"', descripcion='"+proyects.getDescripcion()+"', "
					+ "fecha_inicio=to_date('"+fInicio+"','DD/MM/YYYY'), fecha_fin= to_date('"+fFin+"','DD/MM/YYYY'),estado=(select id_estado from estado_proyecto where descripcion='" + proyects.getEstado()+"')"+" where id_proyecto='"+proyects.getIdProyecto()+"'");
			st.executeUpdate("update proyecto set nombre='"+ proyects.getNombre()+"', descripcion='"+proyects.getDescripcion()+"', "
			+ "fecha_inicio=to_date('"+fInicio+"','DD/MM/YYYY'), fecha_fin= to_date('"+fFin+"','DD/MM/YYYY'),estado=(select id_estado from estado_proyecto where descripcion='" + proyects.getEstado()+"')"+" where id_proyecto='"+proyects.getIdProyecto()+"'");
		}
		System.out.println("4");
		conn.commit();
		conn.close();
	}
	
	public ArrayList<String> getEstado() throws SQLException {
		ArrayList<String> result = new ArrayList<String>();
		ConnectionOracle bd = new ConnectionOracle();
		Connection conn = bd.conectar();
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from estado_proyecto");
		while(rs.next()) {
			result.add(rs.getString("descripcion"));
		}
		return result;
	}

}
