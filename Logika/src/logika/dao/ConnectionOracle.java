package logika.dao;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class ConnectionOracle {
	public String url= "jdbc:oracle:thin:@localhost:1521:XE";
	public String user="is2bd";
	public String pass="is2bd";
	
	public ConnectionOracle() {}
	
	public Connection conectar() {		
		Connection link = null;
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			link = DriverManager.getConnection(this.url, this.user, this.pass);
		}catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e);
		}
		return link;
	}
}
