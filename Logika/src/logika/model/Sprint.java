package logika.model;

import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class Sprint {
	private long idSprint = 0l;
	private Date fechaInicio ;
	private Date fechaFin;
	private Date fechaFinEstimada;
	private String nombre = "";
	private String idProyecto= "";
	private String descProyecto= "";
	
	public Sprint(){		
	}
	
	public long getIdSprint() {
		return idSprint;
	}

	public void setIdSprint(long idStrint) {
		this.idSprint = idStrint;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(String idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getDescProyecto() {
		return descProyecto;
	}

	public void setDescProyecto(String descProyecto) {
		this.descProyecto = descProyecto;
	}

	public Date getFechaFinEstimada() {
		return fechaFinEstimada;
	}

	public void setFechaFinEstimada(Date fechaFinEstimada) {
		this.fechaFinEstimada = fechaFinEstimada;
	}

}