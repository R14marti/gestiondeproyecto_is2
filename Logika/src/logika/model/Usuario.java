package logika.model;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class Usuario{

	private String nombre="";
	private String apellido="";
	private String nickName="";
	private String contrasenha="";
	private String email="";
	private boolean estado = true;
	private String estadoDesc ="";

	public Usuario() {
		
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {		
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getContrasenha() {
		return contrasenha;
	}
	public void setContrasenha(String contrasenha) {
		this.contrasenha = contrasenha;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {		
		this.estado = estado;
		if (this.estado==true) {
			this.estadoDesc="ACTIVO";
		}else{
			this.estadoDesc="INACTIVO";
		}
	}
	
	public String getEstadoDesc() {
		return estadoDesc;
	}

	public void setEstadoDesc(String estadoDesc) {
		this.estadoDesc = estadoDesc;
	}
}