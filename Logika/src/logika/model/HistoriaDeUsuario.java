package logika.model;

import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped

public class HistoriaDeUsuario {


	private int idHistoria =0;
	private String resumen="";
	private String descripcion="";
	private String prioridad="";
	private int estadoKamban=0 ;
	private int idSprint=0;
	private Date fecha_creacion;
	private String idProyecto="";
	private String nombreUsuario="";
	private String rol="";
	
	private String nombreSprint = "";

	
	public String getNombreSprint() {
		return nombreSprint;
	}



	public void setNombreSprint(String nombreSprint) {
		this.nombreSprint = nombreSprint;
	}



	public String getRol() {
		return rol;
	}



	public void setRol(String rol) {
		this.rol = rol;
	}



	public String getIdProyecto() {
		return idProyecto;
	}



	public void setIdProyecto(String idProyecto) {
		this.idProyecto = idProyecto;
	}



	public String getNombreUsuario() {
		return nombreUsuario;
	}



	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}



	public int getIdHistoria() {
		return idHistoria;
	}



	public void setIdHistoria(int idHistoria) {
		this.idHistoria = idHistoria;
	}



	public String getResumen() {
		return resumen;
	}



	public void setResumen(String resumen) {
		this.resumen = resumen;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public String getPrioridad() {
		return prioridad;
	}



	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}



	public int getEstadoKamban() {
		return estadoKamban;
	}



	public void setEstadoKamban(int estadoKamban) {
		this.estadoKamban = estadoKamban;
	}



	public int getIdSprint() {
		return idSprint;
	}



	public void setIdSprint(int idSprint) {
		this.idSprint = idSprint;
	}



	public Date getFecha_creacion() {
		return fecha_creacion;
	}



	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}



	public HistoriaDeUsuario(){
		
	}

	

}
