package logika.backing;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

import com.oracle.jrockit.jfr.Producer;

import logika.dao.DAOUsuario;
import logika.model.Usuario;

@Named
@RequestScoped
public class HomeBean implements Serializable {
	private static final long serialVersionUID = -8030839164392647335L;
	FacesMessage fmessage;
	FacesContext fcontext = FacesContext.getCurrentInstance();
	private LineChartModel sprintBackChart;
	private LineChartModel prodBackChart;
	private PieChartModel pieModel1= new PieChartModel();
	
	public HomeBean() {
		loadCharts();
	}

	private void loadCharts() {
		loadProdBackChart();
		createPieModel1();
	}

	private void loadProdBackChart() {
		LineChartSeries creados = new LineChartSeries(), terminados = new LineChartSeries();
		Axis xAxis = new CategoryAxis("Months");
		Axis yAxis;
		prodBackChart = new LineChartModel();

		yAxis = prodBackChart.getAxis(AxisType.Y);
		xAxis.setLabel("Meses");
        yAxis.setMin(0);
        yAxis.setMax(10);

		prodBackChart.getAxes().put(AxisType.X, xAxis);
		
		creados = generarCantTotal();
		terminados = generarCantTerminados();
		creados.setLabel("Creados");
		terminados.setLabel("Terminados");		
		prodBackChart.addSeries(creados);
		prodBackChart.addSeries(terminados);
		
		prodBackChart.setTitle("DashBoard Product Backlog");
		prodBackChart.setLegendPosition("e");
	}
	
    private void createPieModel1() {
        pieModel1 = new PieChartModel();
 
        pieModel1.set("Por Hacer", 540);
        pieModel1.set("Haciendo", 325);
        pieModel1.set("Terminado", 702);
        pieModel1.setTitle("Sprint Backlog");
        pieModel1.setLegendPosition("w");
        pieModel1.setShadow(false);
    }

	private LineChartSeries generarCantTerminados() {
		LineChartSeries series1 = new LineChartSeries();
		series1.set(1, 2);
        series1.set(2, 1);
        series1.set(3, 3);
        series1.set(4, 6);
        series1.set(5, 8);
		return series1;
	}

	private LineChartSeries generarCantTotal() {
		LineChartSeries series2 = new LineChartSeries();
        series2.set(1, 6);
        series2.set(2, 3);
        series2.set(3, 2);
        series2.set(4, 7);
        series2.set(5, 9);
		return series2;
	}

	private void loadSprintBackChart() {
		LineChartSeries creados = new LineChartSeries(), terminados = new LineChartSeries();
		prodBackChart = new LineChartModel();
		creados.setLabel("Creados");
		terminados.setLabel("terminados");
		sprintBackChart.getAxes().put(AxisType.X, new CategoryAxis("Months"));
		//creados = generarTotal();
		//terminados = generarTotalTerminados();
	
		sprintBackChart.addSeries(creados);
		sprintBackChart.addSeries(terminados);
	}

	public LineChartModel getSprintBackChart() {
		return sprintBackChart;
	}

	public void setSprintBackChart(LineChartModel sprintBackChart) {
		this.sprintBackChart = sprintBackChart;
	}

	public LineChartModel getProdBackChart() {
		return prodBackChart;
	}

	public void setProdBackChart(LineChartModel prodBackChart) {
		this.prodBackChart = prodBackChart;
	}
	
	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.pieModel1 = pieModel1;
	}

}
