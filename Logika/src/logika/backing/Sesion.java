package logika.backing;


import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import logika.dao.DAOUsuario;
import logika.model.Permiso;
import logika.model.Usuario;

@Named
@SessionScoped
public class Sesion implements Serializable{
	private static final long serialVersionUID = -6621429841377887581L;
	private String nickName, password, nombre, apellido;
	private Permiso permiso;

	public Sesion() {
		
	}
		
	public String enviar() {
		String msj="Contraseņa o usuario incorrectos, intentar nuevamente.", clientId=null;
		FacesMessage fmessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, msj, msj);
		FacesContext fcontext = FacesContext.getCurrentInstance();
		
		try {
			if (validarUsuario()){
				return "home";
			}else {
				fcontext.addMessage(clientId, fmessage);
				return "login";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void cerrar() {
		System.out.println("Cerrar Sesion in");
		nickName="";
		password="";
		nombre=""; 
		apellido="";
		permiso = new Permiso();
		System.out.println("Cerrar Sesion ou");
	}
	
	private boolean validarUsuario() throws SQLException {
		boolean result=false;
		DAOUsuario dao = new DAOUsuario();
		
		ArrayList<Usuario> users = dao.getUsuarios();
		for (Usuario u:users) {
			if (nickName.equals(u.getNickName()) && password.equals(u.getContrasenha())) {
				permiso = dao.getPermisoUsuario(nickName);
				this.nombre=u.getNombre();
				this.apellido=u.getApellido();
				result=true;
			}
		}
		return result;
	}
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Permiso getPermiso() {
		return permiso;
	}

	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}
	
}