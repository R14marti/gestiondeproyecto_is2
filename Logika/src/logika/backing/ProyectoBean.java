package logika.backing;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import logika.dao.DAOProyecto;
import logika.model.Proyecto;


@Named
@RequestScoped
public class ProyectoBean implements Serializable {
	private static final long serialVersionUID = 7156139487103883898L;
	private ArrayList<Proyecto> proyecto = new ArrayList<Proyecto>();
	Proyecto seleccionado;
	Proyecto nuevo;
	FacesMessage fmessage;
	FacesContext fcontext = FacesContext.getCurrentInstance();

	public ProyectoBean() {
		seleccionado = new Proyecto();
		nuevo = new Proyecto();
	}

	public void guardar() {
		DAOProyecto dao = new DAOProyecto();	
		try {
			System.out.println("1");
			dao.guardar(nuevo);
			seleccionado = new Proyecto();
			nuevo = new Proyecto();
			System.out.println("5");
		} catch (Exception e) {
			fmessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.toString(), e.toString());
			fcontext.addMessage(null, fmessage);
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> listadoEstado() {
		DAOProyecto dao = new DAOProyecto();
		ArrayList<String> listaEstado = new ArrayList<String>(); 
		
		try {
			listaEstado= dao.getEstado();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		
		}
		return listaEstado;
		
	}

	public ArrayList<Proyecto> getProyecto() {
		DAOProyecto dao = new DAOProyecto();
		try {
			proyecto = dao.getProyecto();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return proyecto;
	}

	public void mostrarSeleccionado() {
		nuevo = seleccionado;
	}

	public void setProyecto(ArrayList<Proyecto> proyecto) {
		this.proyecto = proyecto;
	}

	public Proyecto getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Proyecto seleccionado) {		
		this.seleccionado = seleccionado;
	}

	public Proyecto getNuevo() {
		return nuevo;
	}

	public void setNuevo(Proyecto nuevo) {
		this.nuevo = nuevo;
	}

	

}
