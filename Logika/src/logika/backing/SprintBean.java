package logika.backing;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import logika.dao.DAOSprint;
import logika.model.Sprint;


@Named
@RequestScoped
public class SprintBean implements Serializable {
	private static final long serialVersionUID = 7156139487103883898L;
	private ArrayList<Sprint> sprints = new ArrayList<Sprint>();
	Sprint seleccionado;
	Sprint nuevo;
	FacesMessage fmessage;
	FacesContext fcontext = FacesContext.getCurrentInstance();

	public SprintBean() {
		seleccionado = new Sprint();
		nuevo = new Sprint();
	}

	public void guardar() {
		DAOSprint dao = new DAOSprint();	
		try {
			if (nuevo.getIdSprint() == 0 && seleccionado!=null){
				nuevo.setIdSprint(seleccionado.getIdSprint());
			}
			dao.guardar(nuevo);
			seleccionado = new Sprint();
			nuevo = new Sprint();
		} catch (Exception e) {
			fmessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.toString(), e.toString());
			fcontext.addMessage(null, fmessage);
			e.printStackTrace();
		}
	}

	public ArrayList<Sprint> getSprints() {
		DAOSprint dao = new DAOSprint();
		try {
			sprints = dao.getSprints();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sprints;
	}
	
	public ArrayList<String> completeProyecto(String param) {
		DAOSprint dao = new DAOSprint();
		ArrayList<String> p = new ArrayList<String>();
		try {
			p = dao.completeProyecto(param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	public void mostrarSeleccionado() {
		nuevo = seleccionado;
	}

	public void setSprint(ArrayList<Sprint> sprints) {
		this.sprints = sprints;
	}

	public Sprint getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Sprint seleccionado) {		
		this.seleccionado = seleccionado;
	}

	public Sprint getNuevo() {
		return nuevo;
	}

	public void setNuevo(Sprint nuevo) {
		this.nuevo = nuevo;
	}

}
