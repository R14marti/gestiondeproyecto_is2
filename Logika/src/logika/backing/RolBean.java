package logika.backing;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import logika.dao.DAORol;
import logika.model.Permiso;
import logika.model.Rol;

@Named
@RequestScoped
public class RolBean implements Serializable{
	private static final long serialVersionUID = 1339174451234335683L;
	private ArrayList<Rol> roles = new ArrayList<Rol>();
	private ArrayList<Permiso> permisos = new ArrayList<Permiso>();
	Rol seleccionado= new Rol();
	Rol nuevo =new Rol();
	FacesMessage fmessage;
	FacesContext fcontext = FacesContext.getCurrentInstance();

	public RolBean() {
		DAORol dao = new DAORol();
		try {
			permisos = dao.getPermisos();
			roles = dao.getRoles();
		} catch (SQLException e) {
			fmessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.toString(), e.toString());
			fcontext.addMessage(null, fmessage);
			e.printStackTrace();
		}
	}

	public void guardar() {
		DAORol dao = new DAORol();
		try {
			if (nuevo.getCodigo() == 0 && seleccionado!=null){
				nuevo.setCodigo(seleccionado.getCodigo());
			}
			dao.guardar(nuevo);
			seleccionado = new Rol();
			nuevo = new Rol();
			roles = dao.getRoles();
		} catch (Exception e) {
			fmessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.toString(), e.toString());
			fcontext.addMessage(null, fmessage);
			e.printStackTrace();
		}
	}

	public ArrayList<Rol> getRoles() {
		return roles;
	}

	public void mostrarSeleccionado() {
		this.nuevo = seleccionado;
	}

	public void setRoles(ArrayList<Rol> roles) {
		this.roles = roles;
	}

	public Rol getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Rol seleccionado) {		
		this.seleccionado = seleccionado;
	}

	public Rol getNuevo() {
		return nuevo;
	}

	public void setNuevo(Rol nuevo) {
		this.nuevo = nuevo;
	}

	public ArrayList<Permiso> getPermisos() {
		return permisos;
	}

	public void setPermisos(ArrayList<Permiso> permisos) {
		this.permisos = permisos;
	}
}
