package logika.backing;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import logika.dao.DAOUsuario;
import logika.model.Usuario;

@Named
@RequestScoped
public class UsuarioBean implements Serializable {
	private static final long serialVersionUID = 2415503448970327759L;
	private ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
	Usuario seleccionado;
	Usuario nuevo;
	FacesMessage fmessage;
	FacesContext fcontext = FacesContext.getCurrentInstance();

	public UsuarioBean() {
		DAOUsuario dao = new DAOUsuario();		
		seleccionado = new Usuario();
		nuevo = new Usuario();
		try {
			usuarios = dao.getUsuarios();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void guardar() {
		DAOUsuario dao = new DAOUsuario();	
		try {
			if ((nuevo.getContrasenha()==null || nuevo.getContrasenha().equals("")) && seleccionado!=null) {
				nuevo.setContrasenha(seleccionado.getContrasenha());
			}
			dao.guardar(nuevo);
			seleccionado = new Usuario();
			nuevo = new Usuario();
			usuarios = dao.getUsuarios();
		} catch (Exception e) {
			if (e.toString().contains("CONTRASENHA")) {
				fmessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contraseņa no valida, favor verificar.", "Contraseņa no valida, favor verificar.");
			}else {
				fmessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.toString(), e.toString());				
			}
			fcontext.addMessage(null, fmessage);
			e.printStackTrace();
		}
	}
	
	public ArrayList<Usuario> getUsuarios() {		
		return usuarios;
	}

	public void mostrarSeleccionado() {
		nuevo = seleccionado;
	}

	public void setUsuarios(ArrayList<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Usuario seleccionado) {		
		this.seleccionado = seleccionado;
	}

	public Usuario getNuevo() {
		return nuevo;
	}

	public void setNuevo(Usuario nuevo) {
		this.nuevo = nuevo;
	}

}
