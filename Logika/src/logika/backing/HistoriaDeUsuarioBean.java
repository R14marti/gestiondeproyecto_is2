package logika.backing;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import logika.dao.DAOHistoriaDeUsuario;
import logika.dao.DAOProyecto;
import logika.model.HistoriaDeUsuario;
import logika.model.Proyecto;


@Named
@RequestScoped
public class HistoriaDeUsuarioBean implements Serializable {
	private static final long serialVersionUID = 7156139487103883898L;
	private ArrayList<HistoriaDeUsuario> historiaDeUsuario = new ArrayList<HistoriaDeUsuario>();
	HistoriaDeUsuario seleccionado;
	HistoriaDeUsuario nuevo;
	FacesMessage fmessage;
	FacesContext fcontext = FacesContext.getCurrentInstance();

	public HistoriaDeUsuarioBean() {
		seleccionado = new HistoriaDeUsuario();
		nuevo = new HistoriaDeUsuario();
	}

	public void guardar() {
		System.out.println("4");
		DAOHistoriaDeUsuario dao = new DAOHistoriaDeUsuario();	
		try {
			System.out.println("5");
			dao.guardar(nuevo);
			seleccionado = new HistoriaDeUsuario();
			nuevo = new HistoriaDeUsuario();
		} catch (Exception e) {
			fmessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.toString(), e.toString());
			fcontext.addMessage(null, fmessage);
			e.printStackTrace();
		}
	}

	public ArrayList<HistoriaDeUsuario> getHistoriaDeUsuario() {
		DAOHistoriaDeUsuario dao = new DAOHistoriaDeUsuario();
		try {
			historiaDeUsuario = dao.getHistoriaDeUsuario();
			System.out.println("historiaDeUsuario " + historiaDeUsuario);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return historiaDeUsuario;
	}
	
	public ArrayList<String> getNombreSprint() {
		DAOHistoriaDeUsuario dao = new DAOHistoriaDeUsuario();
		ArrayList<String> sprintlist = new ArrayList<String>();
		try {
			sprintlist=dao.getNombreSprint(); 
			System.out.println("getNombreSprint " + sprintlist);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sprintlist;
	}
	
	public ArrayList<String> getNombreProyecto() {
		DAOHistoriaDeUsuario dao = new DAOHistoriaDeUsuario();
		ArrayList<String> sprintlist = new ArrayList<String>();
		try {
			sprintlist=dao.getNombreProyecto(); 
			System.out.println("getNombreProyecto " + sprintlist);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sprintlist;
	}
	
	public ArrayList<String> getResponsable() {
		DAOHistoriaDeUsuario dao = new DAOHistoriaDeUsuario();
		ArrayList<String> sprintlist = new ArrayList<String>();
		try {
			sprintlist=dao.getResponsable(); 
			System.out.println("getResponsable " + sprintlist);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sprintlist;
	}

	public ArrayList<String> getRol() {
		DAOHistoriaDeUsuario dao = new DAOHistoriaDeUsuario();
		ArrayList<String> sprintlist = new ArrayList<String>();
		try {
			sprintlist=dao.getRol(); 
			System.out.println("getRol " + sprintlist);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sprintlist;
	}
	
	public void mostrarSeleccionado() {
		nuevo = seleccionado;
	}

	public void setHistoriaDeUsuario(ArrayList<HistoriaDeUsuario> historiaDeUsuario) {
		this.historiaDeUsuario = historiaDeUsuario;
	}

	public HistoriaDeUsuario getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(HistoriaDeUsuario seleccionado) {		
		this.seleccionado = seleccionado;
	}

	public HistoriaDeUsuario getNuevo() {
		return nuevo;
	}

	public void setNuevo(HistoriaDeUsuario nuevo) {
		this.nuevo = nuevo;
	}

	
}
